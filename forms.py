from django import forms

class ReportForm(forms.Form):
	"""Create form fields for searching records."""
    start_date = forms.CharField(widget=forms.TextInput(attrs={'class': 'date validate[required] start', 'placeholder': 'mm/dd/yyyy'}))
    end_date = forms.CharField(widget=forms.TextInput(attrs={'class': 'date validate[required] end', 'placeholder': 'mm/dd/yyyy'}))
    records = forms.CharField(widget=forms.TextInput(attrs={'class': 'records validate[required]', 'placeholder': 'Qty'}))
    order = forms.ChoiceField(widget=forms.Select, choices=(('DESC', 'Descending'), ('ASC', 'Ascending')))