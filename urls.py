from django.conf.urls import patterns, url
from belladreamhair.reports import views

urlpatterns = patterns('',
    url("^(?P<report>.*)/csv/$", views.report_csv, name="csv"), # Returns a CSV file of the report
    url("^(?P<report>.*)/$", views.report, name="report"), # Returns the report view
    url('^$', views.index, name='reports'), # Lists available reports
)