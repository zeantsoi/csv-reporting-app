import csv
import datetime
import re

from cartridge.shop.models import Order
from django.conf.urls import patterns
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.template import Context, loader
from django.utils import formats, timezone
from django.utils.dateformat import DateFormat
from django.utils.formats import get_format
from mezzanine.utils.views import render

from belladreamhair.reports.forms import ReportForm


@staff_member_required # Decorator that ensures that only a logged in admin can access
def index(request):
	"""Renders the index template."""
	return render(request, 'reports.html')


@staff_member_required
def report_csv(request, report):
	"""Generates CSV file containing report content_type.
	Creates HttpResponse object with file content-disposition header.
	Iterates through each row in results and writes to the CSV.
	"""
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="report.csv"'

	writer = csv.writer(response)
	for row in results(report):
		writer.writerow([row.description, row.sum])

	return response


@staff_member_required
def report(request, report):
	"""Retrieves the report data and renders it in the report.html template.
	First, checks whether the request.method is 'POST',
	if so, it passes the form inputs as arguments to the results() function.
	Otherwise, it sets defaults for start_date and end_date
	and returns results based on defaults.
	Renders context to the report.html template.
	"""
	if request.method == 'POST':
		start_date = request.POST.get('start_date')
		end_date = request.POST.get('end_date')
		records = request.POST.get('records')
		order = request.POST.get('order')
	
		if 'csv' in request.POST:
			response = HttpResponse(content_type='text/csv')
			response['Content-Disposition'] = 'attachment; filename="report.csv"'

			writer = csv.writer(response)
			for row in results(report, start_date, end_date, records, order):
				writer.writerow([row.description, row.sum])

			return response

	else:		
		try:
			start_date = Order.objects.filter(status='2').order_by('time')[0].time.strftime("%m/%d/%Y")
			end_date = datetime.datetime.combine(timezone.now(), datetime.time(0)).strftime("%m/%d/%Y")
		except: # If there are no entries to iterate through
			start_date = datetime.datetime.now().strftime("%m/%d/%Y")
			end_date = datetime.datetime.now().strftime("%m/%d/%Y")

		records = '20'
		order = 'DESC'		

	context = {
		'start_date': start_date,
		'end_date': end_date,
		'records': records,
		'report': report,
		'order': order,
		'rows': results(report, start_date, end_date, records, order),
		'report_form': ReportForm,
	}

	return render(request, 'report.html', context)


def results(report, start_date, end_date, records, order):
	"""Return results of query based on passed arguments.
	Performs regex find on date input; if it evaluates,
	it passes pieces together the dat in yy/mm/dd format.
	Then, does a raw SQL query with summations.
	"""
	if re.findall(r"\b\d{1,2}[-/:]\d{1,2}[-/:]\d{4}\b", str(start_date)):
		start_date = start_date.split('/')[2] + '-' + start_date.split('/')[0] + '-' + start_date.split('/')[1]

	if re.findall(r"\b\d{1,2}[-/:]\d{1,2}[-/:]\d{4}\b", str(end_date)):
		end_date = end_date.split('/')[2] + '-' + end_date.split('/')[0] + '-' + end_date.split('/')[1] + ' 23:59:59'

	if report == 'orders':
		"""Returns total amount of each product sold.
		Queries orderitems joined to an order by
		1) status, 
		2) date range
		"""
		return Order.objects.raw('SELECT orderitem.id, orderitem.description as description, SUM(orderitem.total_price) AS sum, SUM(shop_order.tax_total) AS tax_total FROM shop_orderitem AS orderitem INNER JOIN shop_order ON shop_order.id = orderitem.order_id WHERE shop_order.status = 2 AND shop_order.time > "' + start_date + '" AND shop_order.time < "' + end_date + '" GROUP BY orderitem.description ORDER BY SUM(orderitem.total_price) ' + order + ' LIMIT ' + records + '')

	if report == 'states':
		"""Returns tax totals by state.
		Queries orders by state and
		1) status, 
		2) date range
		"""
		return Order.objects.raw('SELECT id, shipping_detail_state as description, SUM(item_total) AS sum, SUM(tax_total) AS tax_total FROM shop_order WHERE status = 2 AND time > "' + start_date + '" AND time < "' + end_date + '" GROUP BY shipping_detail_state ORDER BY SUM(item_total) ' + order + ' LIMIT ' + records + '')

	if report == 'customers':
		"""Returns tax totals by state.
		Queries orders by 
		1) status, 
		2) date range
		Then groups by email.
		"""
		return Order.objects.raw('SELECT id, billing_detail_email as description, SUM(item_total) AS sum FROM shop_order WHERE status = 2 AND time > "'  + start_date + '" AND time < "' + end_date + '" GROUP BY billing_detail_email ORDER BY SUM(item_total) ' + order + ' LIMIT ' + records + '')
